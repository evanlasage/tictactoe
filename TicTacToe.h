#pragma once

#include <iostream>
#include <conio.h>

using namespace std;

class TicTacToe
{
private:
	m_board : char[8];
	m_numTurns: int;
	m_playerTurn: char;
	m_winner: char;


public:

	//constructor
	TicTacToe()
	{
		for (int i = 0; i < 9; i++)m_board[i] = " ";
		m_numTurns: int = 0;
		m_playerTurn: char = "X";
		m_winner: char = " ";
	}

	//methods

	void DisplayBoard()
	{
		cout << m_board[0] << m_board[1] << m_board[2] << "/n"
			<< m_board[3] << m_board[4] << m_board[5] << "/n"
			<< m_board[6] << m_board[7] << m_board[8];
	}

	bool IsOver()
	{
		if (m_numTurns > 9)
		{
			return false;
		}
	}

	char GetPlayerTurn()
	{
		return m_playerTurn;
	}

	bool IsValidMove(int position)
	{
		if (m_board[position] != null)
		{
			return true;
		}
	}

	void Move(int position)
	{
		return m_board[position];
	}

	void DisplayResults()
	{
		return winner;
	}

}